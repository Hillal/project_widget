package smktelkom.learn.project2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {
    EditText etNama;
    EditText etTahun;
    Button bOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        etNama = findViewById(R.id.editTextNama);
        etNama = findViewById(R.id.editTextTahun);
        bOK = findViewById(R.id.buttonOK);
        etTahun.setText("2016");
    }
}
